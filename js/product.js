function extend(Child, Parent) { //func for parent class prototype inheritance
    Child.prototype = Object.create(Parent.prototype);
    Child.prototype.constructor = Child;
    Child.superclass = Parent.prototype;
}
let AbstractProduct = function (obj) { // abstract class
    if (this.constructor === AbstractProduct) {
        throw new Error('Cannot instantiate abstract class');
    }
    const defPar = {
        ID: (Math.round(Date.now() * Math.random()) * -1).toString(),
        name: 'no name',
        description: 'no description',
        price: 0,
        images: ['https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/480px-No_image_available.svg.png']
    }
    const resProp = Object.assign(defPar, obj);
    this.ID = resProp.ID,
    this.name = resProp.name,
    this.description = resProp.description,
    this.price = resProp.price,
    this.images = resProp.images
};

AbstractProduct.prototype.getID = function () {
    return this.ID;
};

AbstractProduct.prototype.getName = function () {
    return this.name;
};

AbstractProduct.prototype.getDescription = function () {
    return this.description;
};

AbstractProduct.prototype.getPrice = function () {
    return this.price;
};

AbstractProduct.prototype.getBrand = function () {
    return this.brand;
};

AbstractProduct.prototype.getQuantity = function () {
    return this.quantity;
};

AbstractProduct.prototype.getDate = function () {
    return this.date;
};

AbstractProduct.prototype.getReviews = function () {
    return this.reviews;
};

AbstractProduct.prototype.getReviewByID = function (id) {
    return this.reviews.find(elem => elem.ID === id);
};

AbstractProduct.prototype.getImages = function () {
    return this.images;
};

AbstractProduct.prototype.getImage = function (param = 0) {
    if (param >= 0 && param <= this.images.length) {
        return this.images[param];
    }
};

AbstractProduct.prototype.setName = function (newName) {
    this.name = newName;
};

AbstractProduct.prototype.setDescription = function (newDescription) {
    this.description = newDescription;
};

AbstractProduct.prototype.setPrice = function (newPrice) {
    this.price = newPrice;
};

AbstractProduct.prototype.setBrand = function (newBrand) {
    this.brand = newBrand;
};

AbstractProduct.prototype.setDate = function (newDate) {
    this.date = newDate;
};

AbstractProduct.prototype.addReview = function (review) {
    this.reviews.push(review);
};

AbstractProduct.prototype.deleteReview = function (param) {
    const delIndex = this.reviews.findIndex((elem) => param === elem.ID ? true : false);
    console.log(delIndex);
    if (delIndex >= 0) {
        this.reviews.splice(delIndex, 1);
    }

};

AbstractProduct.prototype.getAverageRating = function () {
    let avgRating = 0;
    let allReviews = this.reviews.length * 4;
    this.reviews.forEach(elem => {
        elem.rating.forEach(ratNumb => {
            avgRating += ratNumb;
        })
    })
    return (avgRating / allReviews).toFixed(2);
}

AbstractProduct.prototype.getFullInformation = function () {
    let resultStr = '';
    for (let property in this) {
        if (this.hasOwnProperty(property)) {
            if (property === 'reviews') {
                resultStr += `${property} : ${this[property].length}\n`;
            } else {
                resultStr += `${property} : ${this[property]}\n`;
            }
        }
    }
    return resultStr;
};

AbstractProduct.prototype.getPriceForQuantity = function (quantity) {
    return `$${(quantity * this.price).toFixed(2)}`;
};

AbstractProduct.prototype.property = function (prop, value) {
    const type = arguments.length < 2 ? 'get' : 'set';
    if (this.hasOwnProperty(prop)) {
        if (type === 'get') {
            return this[prop];
        } else if (type === 'set') {
            this[prop] = value;
        }
    }
};
AbstractProduct.prototype.getProductTileHTML = function () {

    let li = document.createElement('li');
    li.classList.add('products__item');

    let neededProp = [
        'ID',
        'name',
        'description',
        'price',
        'images',
        'brand',
        'quantity',
        /* Clothes */
        'sizes',
        'activeSize',
        'material',
        'color',
        /* Electronics */
        'warranty',
        'power'
    ]

    for (let property in this) {

        if (this.hasOwnProperty(property) && neededProp.includes(property)) {

            const propInfo = document.createElement('span');
            let propValue;
            const propValueDiv = document.createElement('div');
            propValueDiv.classList.add('case__value');
            const propInfoDiv = document.createElement('div');
            propInfoDiv.classList.add('case__info');
            const propDiv = document.createElement('div');
            propDiv.classList.add('product__case');

            if (property === 'images') {
                propValue = document.createElement('img');
                propValue.classList.add('product__img');
                propValue.src = this[property];
                li.prepend(propDiv);
            } else {
                propValue = document.createElement('span');
                propValue.dataset.caseValue = `${property}`;
                propValue.innerText = this.property(property);
                li.append(propDiv);
            }

            propInfo.innerText = property;
            propValueDiv.append(propValue);
            propInfoDiv.append(propInfo);
            propDiv.append(propInfoDiv, propValueDiv);

        }
    }
    return li;

}

var plp = (function (my) {

    my.getProductsJSONData = function () {
        return $.get('./product-feed.json');

    }
    my.renderProducts = function (products) {
        products.forEach(elem => {
            let mainUl = $('.js-products');
            let li = elem.getProductTileHTML();
            mainUl.append(li);
        })
        
    }
    return my;
}(plp || {}));


function Clothes(obj) {
    AbstractProduct.call(this, obj); //property inheritance

    const defaultParam = {
        brand: 'no brand',
        sizes: 'no array of sizes',
        activeSize: 'no activeSize',
        quantity: -1,
        date: new Date(),
        reviews: [],
        material: 'no material',
        color: 'no color'
    };
    const resObj = Object.assign(defaultParam, obj);

    this.brand = resObj.brand,
    this.sizes = resObj.sizes,
    this.activeSize = resObj.activeSize,
    this.quantity = resObj.quantity,
    this.date = resObj.date,
    this.reviews = resObj.reviews,
    this.material = resObj.material,
    this.color = resObj.color

};

extend(Clothes, AbstractProduct);

Clothes.prototype.getSizes = function () {
    return this.sizes;
};

Clothes.prototype.getActiveSize = function () {
    return this.activeSize;
};

Clothes.prototype.getMaterial = function () {
    return this.material;
};

Clothes.prototype.getColor = function () {
    return this.color;
};

Clothes.prototype.setMaterial = function (newMaterial) {
    this.material = newMaterial;
};

Clothes.prototype.setColor = function (newColor) {
    this.color = newColor;
};

Clothes.prototype.addSize = function (size) {
    this.sizes.push(size);
};

Clothes.prototype.deleteSize = function (size) {
    this.sizes = this.sizes.filter(elem => elem !== size);
};

Clothes.prototype.setActiveSize = function (newActSize) {
    this.activeSize = newActSize;
};


function Electronics(obj) {
    AbstractProduct.call(this, obj); //property inheritance

    const defaultParam = {
        brand: 'no brand',
        quantity: -1,
        date: new Date(),
        reviews: [],
        warranty: -1,
        power: -1
    };
    const resObj = Object.assign(defaultParam, obj);

    this.brand = resObj.brand,
    this.quantity = resObj.quantity,
    this.date = resObj.date,
    this.reviews = resObj.reviews,
    this.warranty = resObj.warranty,
    this.power = resObj.power

};

extend(Electronics, AbstractProduct);

Electronics.prototype.getWrranty = function () {
    return this.warranty;
};

Electronics.prototype.getPower = function () {
    return this.power;
};

Electronics.prototype.setWarranty = function (newWarranty) {
    if (newWarranty <= 10 && newWarranty > 0) {
        this.warranty = newWarranty;
    }
};

Electronics.prototype.setPower = function (newPower) {
    this.power = newPower;
};

let Validator = {
    validateEmail: function (email) {
        let reg = /^([^\-\.][A-Za-z0-9\-\.]{1,20}\@)+([A-Za-z0-9\.!$%&amp;’*+/=?^_-]{1,15})+\.([A-Za-z]{1,5})$/;

        return reg.test(email) ? 'valid' : 'not valid';
    },

    validatePhone: function (phone) {
        let reg = /^(?=^.{0,25}$)((([\- ]*[\+38]){3})?[\- ]*[\(]?([\- ]*[0-9]){3}[\- ]*[\)]?([\- ]*[0-9]){7}[\- ]*)$/;

        return reg.test(phone) ? 'valid' : 'not valid';
    },
    validatePassword: function (password) {
        let reg = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[\.!$%&amp;’*+/=?^_-]).{8,}$/;

        return reg.test(password) ? 'valid' : 'not valid';
    },
    validateSearch: function (searchValue) {
        let reg = /^([A-Za-z][\*]?){3,30}$/

        return reg.test(searchValue);
    }
}

/* //used to test Validator
//valid
console.log(Validator.validatorPassword('C00l_Pass'));
console.log(Validator.validatorPassword('SupperPas@1'));
//not valid
console.log(Validator.validatorPassword('Cool_pass'));
console.log(Validator.validatorPassword('C00l'));

//valid
console.log(Validator.validatePhone('+38 (099) 567 8901'));
console.log(Validator.validatePhone('+38 099 5 6 7 8 9 01'));
console.log(Validator.validatePhone('(09-9   ) 567-890-1'));
console.log(Validator.validatePhone('-- (099) 567 89 0-1      '));
//not valid
console.log(Validator.validatePhone('+38 (099) 567 8901 023 4 23423 4234 234 234 234 234 '));
console.log(Validator.validatePhone('+38 099 a0000000'));
console.log(Validator.validatePhone('+38 (0989) 567 8901'));
console.log(Validator.validatePhone('+48 (0989) 567 8901'));

//valid
console.log(Validator.validateEmail('fi@secondpart.end'));
console.log(Validator.validateEmclothes3.getProductTileHTML();
clothes3.getProductTileHTML();
clothes3.getProductTileHTML();ail('first-part@.se=cond%p.art.end'));
console.log(Validator.validateEmail('first.part@se=cond%part.r'));
//not valid
console.log(Validator.validateEmail('f@secondart.end,'));
console.log(Validator.validateEmail('first-part@.se=cond@part.end'));
console.log(Validator.validateEmail('-firstpart@.se=cond%.enddeded'));
console.log(Validator.validateEmail('firs_tpart@.se.en'));
console.log(Validator.validateEmail('firstpart@.se.enddeded')); */


function searchProducts(products, search) {
    return products.filter((elem) => {
        return elem.name.toLowerCase().includes(search.toLowerCase()) || elem.description.toLowerCase().includes(search.toLowerCase());
    })
}

function sortProducts(products, sortRule, howRule = 'asce') {
    let resArr;
    switch (typeof products[0][sortRule]) {
        case 'number':
            resArr = products.sort((a, b) => {
                if (howRule === 'desc') {
                    return b[sortRule] - a[sortRule]
                } else {
                    return a[sortRule] - b[sortRule];
                }
            })
            return resArr;
        case 'string':
            resArr = products.sort((a, b) => {
                let nameA = a[sortRule].toUpperCase();
                let nameB = b[sortRule].toUpperCase();
                if (howRule === 'desc') {
                    return nameB.localeCompare(nameA);
                } else {
                    return nameA.localeCompare(nameB);
                }
            })
            return resArr;
    }
    if (sortRule === 'rating') {
        resArr = products.sort((a, b) => {
            if (howRule === 'desc') {
                return b.getAverageRating() - a.getAverageRating();
            } else {
                return a.getAverageRating() - b.getAverageRating();;
            }
        })
        return resArr;
    } else if (sortRule === 'date') {
        resArr = products.sort((a, b) => {
            if (howRule === 'desc') {
                return b.date.getTime() - a.date.getTime();
            } else {
                return a.date.getTime() - b.date.getTime();
            }
        })
        return resArr;
    }
}

$(document).ready(function () {
    const cArr = plp.getProductsJSONData();
    cArr.then((data) => {
        let clothArr = [];
        data.forEach((elem) => {
            cloth = new Clothes(elem);
            clothArr.push(cloth);
        });
        plp.renderProducts(clothArr);
    });

    const $searchInput = $('.js-search__input');
    $searchInput.on('input', function () {
        const $wrongValDiv = $('.js-wrong-value');
        if (Validator.validateSearch($(this).val())) {
            $wrongValDiv.css('display', 'none');
            $(this).css('border-color', 'blue');
        } else {
            $wrongValDiv.css('display', 'block');
            $(this).css('border-color', 'red');
        }
    })

    $('.js-product__search').on('submit', function (event) {
        event.preventDefault();
        if (Validator.validateSearch($searchInput.val())) {

            const searchResult = searchProducts(clothesArr, $searchInput.val());
            const $products = $('.js-products');
            if (searchResult.length === 0) {
                let empty = new Clothes();
                let li = empty.getProductTileHTML();
                $products.prepend(li);
            } else {
                searchResult.forEach((elem) => {
                    let li = elem.getProductTileHTML();
                    $products.prepend(li);
                })
            }
            $searchInput.val('');
        }
    })

    $('.js-price__select').on('change', function () {

        const $nodeList = $('.products__item');
        let itemsArr = [];
        const parent = $nodeList.parent();
        const sortRule = $(this).val();

        $nodeList.each(function () {
            parent.remove($(this));
            itemsArr.push($(this));
        })
        itemsArr.sort(function (nodeA, nodeB) {
            let valueA = nodeA.find('[data-case-value="price"]').text();
            let valueB = nodeB.find('[data-case-value="price"]').text();
            valueA = Number(valueA);
            valueB = Number(valueB);
            if (sortRule === 'desc') {
                return valueB - valueA;
            } else {
                return valueA - valueB;
            }
        })
            .forEach(function (node) {
                parent.append(node);
            });
    });
    $('.js-name__select').on('change', function () {
        const $nodeList = $('.products__item');
        let itemsArr = [];
        const parent = $nodeList.parent();
        const sortRule = $(this).val();
        
        $nodeList.each(function () {
            parent.remove($(this));
            itemsArr.push($(this));
        })
        itemsArr.sort(function (nodeA, nodeB) {
            let valueA = nodeA.find('[data-case-value="name"]').text();
            let valueB = nodeB.find('[data-case-value="name"]').text();
            valueA = valueA.toUpperCase();
            valueB = valueB.toUpperCase();
            
            if (sortRule === 'desc') {
                return valueB.localeCompare(valueA);
            } else {
                return valueA.localeCompare(valueB);
            }
        })
            .forEach(function (node) {
                parent.append(node);
            });
    })
});

/* used to test assignments */

let clothes1 = new Clothes({
    ID: '1411',
    name: 't-shirt',
    description: 'black t-shirt with a white inscription on the front side',
    price: 799,
    brand: 'Puma',
    sizes: ['XS', 'M', 'XL'],
    activeSize: 'M',
    quantity: 11,
    date: new Date(2021, 11, 07, 09, 31, 48),
    reviews: [
        { ID: '221', author: 'jeka', date: '2019-08-19 22:16:43', comment: 'sho eto?', rating: new Map([['service', 5], ['price', 5], ['value', 5], ['quality', 5]]) },
        { ID: '369', author: 'Dima', date: '2020-03-21 15:09:31', comment: 'kuda?', rating: new Map([['service', 2], ['price', 3], ['value', 1], ['quality', 2]]) },
        { ID: '131', author: 'Viktor', date: '2020-12-14 09:22:27', comment: 'suda?', rating: new Map([['service', 5], ['price', 4], ['value', 3], ['quality', 5]]) }
    ],
    images: ['https://a.lmcdn.ru/img600x866/P/U/PU053EBEGCY1_8992422_1_v1.jpg'],
    material: 'cotton',
    color: 'black'
});
/* document.querySelector('.js-products').append(clothes1.getProductTileHTML()); */

let clothes2 = new Clothes({
    ID: '925',
    name: 'jacket',
    description: 'long-sleeved red sports jacket',
    price: 1560,
    brand: 'Nike',
    sizes: ['S', 'M', 'L'],
    activeSize: 'S',
    quantity: 19,
    date: new Date(2019, 06, 19, 14, 10, 11),
    reviews: [
        { ID: '221', author: 'jeka', date: '2019-08-19 22:16:43', comment: 'sho eto?', rating: new Map([['service', 1], ['price', 1], ['value', 1], ['quality', 1]]) },
        { ID: '369', author: 'Dima', date: '2020-03-21 15:09:31', comment: 'kuda?', rating: new Map([['service', 2], ['price', 3], ['value', 1], ['quality', 2]]) },
        { ID: '131', author: 'Viktor', date: '2020-12-14 09:22:27', comment: 'suda?', rating: new Map([['service', 5], ['price', 4], ['value', 3], ['quality', 5]]) }
    ],
    images: ['https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRd7UPuVdpR-hTIAQuQhf2hBPWzmKPZ3Oeqe6tUGJ8gSD_SJPlzCfVTxTdfDl2vNvX-fWOtUE4&usqp=CAc'],
    material: 'leatherette',
    color: 'red'
});
/* clothes2.getProductTileHTML(); */

let clothes3 = new Clothes({
    ID: '1818',
    name: 'jeans',
    description: 'beautiful stylish skinny jeans in black',
    price: 1399,
    brand: 'Levis',
    sizes: ['XS', 'M', 'L', 'XL'],
    activeSize: 'XL',
    quantity: 7,
    date: new Date(2020, 03, 15, 12, 19, 51),
    reviews: [
        { ID: '221', author: 'jeka', date: '2019-08-19 22:16:43', comment: 'sho eto?', rating: new Map([['service', 3], ['price', 3], ['value', 3], ['quality', 3]]) },
        { ID: '369', author: 'Dima', date: '2020-03-21 15:09:31', comment: 'kuda?', rating: new Map([['service', 2], ['price', 3], ['value', 1], ['quality', 2]]) },
        { ID: '131', author: 'Viktor', date: '2020-12-14 09:22:27', comment: 'suda?', rating: new Map([['service', 5], ['price', 4], ['value', 3], ['quality', 5]]) }
    ],
    images: ['https://lsco.scene7.com/is/image/lsco/levis/clothing/005010165-front-pdp.jpg?$regular_mobile$'],
    material: 'leather',
    color: 'black'
});
/* clothes3.getProductTileHTML();
clothes3.getProductTileHTML();
clothes3.getProductTileHTML(); */

let clothesArr = [clothes1, clothes2, clothes3];
// console.log(sortProducts(clothesArr,'price','desc')); 

let electronic1 = new Electronics({
    ID: '1596',
    name: 'TV',
    description: 'Телевизор без рамок, большая диагональ, 4К, черного цвета',
    price: 18499,
    brand: 'Sony',
    quantity: 11,
    date: new Date(2017, 03, 09, 01, 33, 42),
    reviews: [
        { ID: '221', author: 'jeka', date: '2019-08-19 22:16:43', comment: 'sho eto?', rating: new Map([['service', 5], ['price', 5], ['value', 5], ['quality', 5]]) },
        { ID: '369', author: 'Dima', date: '2020-03-21 15:09:31', comment: 'kuda?', rating: new Map([['service', 2], ['price', 3], ['value', 1], ['quality', 2]]) },
        { ID: '131', author: 'Viktor', date: '2020-12-14 09:22:27', comment: 'suda?', rating: new Map([['service', 5], ['price', 4], ['value', 3], ['quality', 5]]) }
    ],
    images: ['./img0', './img1', './img2', './img3', './img4'],
    warranty: 7,
    power: 320
});
/* electronic1.getProductTileHTML(); */
let electronic2 = new Electronics({
    ID: '1936',
    name: 'Laptop',
    description: 'Качественный ноутбук, красного цвета',
    price: 12199,
    brand: 'Acer',
    quantity: 11,
    date: new Date(2019, 03, 09, 01, 33, 42),
    reviews: [
        { ID: '221', author: 'jeka', date: '2019-08-19 22:16:43', comment: 'sho eto?', rating: new Map([['service', 5], ['price', 5], ['value', 5], ['quality', 5]]) },
        { ID: '369', author: 'Dima', date: '2020-03-21 15:09:31', comment: 'kuda?', rating: new Map([['service', 2], ['price', 3], ['value', 1], ['quality', 2]]) },
        { ID: '131', author: 'Viktor', date: '2020-12-14 09:22:27', comment: 'suda?', rating: new Map([['service', 5], ['price', 4], ['value', 3], ['quality', 5]]) }
    ],
    images: ['./img0', './img1', './img2', './img3', './img4'],
    warranty: 15,
    power: 220
});
let electronic3 = new Electronics({
    ID: '209',
    name: 'Monitor',
    description: 'Современный монитор с хорошими характеристиками, зеленого цвета',
    price: 7999,
    brand: 'Apple',
    quantity: 11,
    date: new Date(2020, 03, 09, 01, 33, 42),
    reviews: [
        { ID: '221', author: 'jeka', date: '2019-08-19 22:16:43', comment: 'sho eto?', rating: new Map([['service', 5], ['price', 5], ['value', 5], ['quality', 5]]) },
        { ID: '369', author: 'Dima', date: '2020-03-21 15:09:31', comment: 'kuda?', rating: new Map([['service', 2], ['price', 3], ['value', 1], ['quality', 2]]) },
        { ID: '131', author: 'Viktor', date: '2020-12-14 09:22:27', comment: 'suda?', rating: new Map([['service', 5], ['price', 4], ['value', 3], ['quality', 5]]) }
    ],
    images: ['./img0', './img1', './img2', './img3', './img4'],
    warranty: 2,
    power: 280
});
let electronicsArr = [electronic1, electronic2, electronic3];

//console.log(electronic2);
//console.log(electronic2.property('name'));
//electronic2.property('ID','122');
//console.log(electronic2);
// console.log(electronic2.property('get','power'));


// 144,263,269





